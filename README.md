# Moved to <https://github.com/rusty-snake/kyst>

<br>


Keep Your Sandbox Tight!
========================

Actually just a collection of my own tight sandbox configurations.

---------------------------------------------------------------------

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.
