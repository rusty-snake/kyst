Keep Your FireJail-Sandbox Tight
================================

These files are supposed to reside in `~/.config/firejail`.

---------------------------------------------------------------------

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.
