# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

blacklist ${HOME}/.java

blacklist ${PATH}/java
blacklist /usr/lib/java
blacklist /etc/java
blacklist /usr/share/java
