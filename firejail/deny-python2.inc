# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

blacklist ${PATH}/python2*
blacklist /usr/include/python2*
blacklist /usr/lib/python2*
blacklist /usr/local/lib/python2*
blacklist /usr/share/python2*
