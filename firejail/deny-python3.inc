# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

blacklist ${PATH}/python3*
blacklist /usr/include/python3*
blacklist /usr/lib/python3*
blacklist /usr/local/lib/python3*
blacklist /usr/share/python3*
