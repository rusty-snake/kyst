# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

# Firejail profile for mpv
# Description: Video player based on MPlayer/mplayer2
# This file is overwritten after every install/update
quiet
# Persistent local customizations
include mpv.local
# Persistent global definitions
include globals.local

ignore nodvd

noblacklist ${MUSIC}
noblacklist ${PICTURES}
noblacklist ${VIDEOS}

net none
protocol unix

include mpv-common.profile
